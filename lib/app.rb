$:.unshift File.expand_path(File.dirname(__FILE__))
require 'sinatra'
require 'sinatra/cross_origin'
require 'my_2048_score'
require 'url_shortener'
require 'json'

configure do
  enable :cross_origin
end

before do
  response.headers["Access-Control-Allow-Origin"] = "*"
end

helpers do
  def check_body
    body = request.body.read
    params.merge! JSON.parse(body) unless (body.nil? || body.empty?)
  end
end

post '/2048/highscores/:player' do
  check_body
  player = params[:player]
  score = params['score']
  won = params['won'].to_s
  My2048Score.new.save_score(player, score, won)
  201
end

options '/2048/highscores/:player' do
  200
end

get '/2048/highscores/:player' do
  scores = My2048Score.new.get_last_scores(params[:player], params[:count] || 5)
  [200, {scores: scores}.to_json]
end

get '/2048/highscores/:player/win' do
  [200, {win: My2048Score.new.get_win_count(params[:player])}.to_json]
end

get '/2048/highscores/:player/loss' do
  [200, {loss: My2048Score.new.get_loss_count(params[:player])}.to_json]
end

post '/s' do
  check_body
  [201, {url: "#{request.base_url}/#{UrlShortener.set(params['url'])}"}.to_json] unless params['url'].nil? || params['url'].empty?
end

options '/s' do
  200
end

get '/:key' do
  url = UrlShortener.get(params[:key])
  if url
    redirect url, 301
  else
    404
  end
end
