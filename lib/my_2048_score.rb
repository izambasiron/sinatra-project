require 'redis'

class My2048Score

  def initialize
    @redis = Redis.new
  end

  def save_score(player, score, won)
    @redis.rpush "2048:#{player}_score", score
    if won.to_s === 'true' # was converted to string
      @redis.incr "2048:#{player}_win"
    else
      a = @redis.incr "2048:#{player}_loss"
    end
  end

  def get_win_count(player)
    count = @redis.get("2048:#{player}_win") || 0
    count.to_i
  end

  def get_loss_count(player)
    count = @redis.get("2048:#{player}_loss") || 0
    count.to_i
  end

  def get_last_scores(player, count = 1)
    scores = @redis.lrange("2048:#{player}_score", -count, -1)
    scores = [0] unless scores.any?
    scores.collect(&:to_i)
  end

end
