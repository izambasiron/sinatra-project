require 'redis'

class UrlShortener
  @@redis = nil

  def initialize
    @@redis = Redis.new
  end

  def self.set(long_url, length = 5)
    redis = @@redis || Redis.new
    key = rand(36**length).to_s(36)
    redis.set key, long_url
    key
  end

  def self.get(key)
    redis = @@redis || Redis.new
    redis.get(key)
  end

end
