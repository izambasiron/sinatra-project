require 'fakeredis'
require 'my_2048_score'
require 'test/unit'
require 'rack/test'

class My2048Test < Test::Unit::TestCase

  def test_loss_game
    Redis.new.flushdb
    game = My2048Score.new
    game.save_score(:ai, 100, 'false')
    assert_equal 0, game.get_win_count(:ai)
    assert_equal 1, game.get_loss_count(:ai)
    assert_equal [100], game.get_last_scores(:ai)
  end

  def test_won_game
    game = My2048Score.new
    game.save_score(:ai, 1001, 'true')
    assert_equal 1, game.get_win_count(:ai)
    assert_equal [100, 1001], game.get_last_scores(:ai, 2)
  end

end
