require 'fakeredis'
require 'url_shortener'
require 'test/unit'
require 'rack/test'

class URLShortenerTest < Test::Unit::TestCase

  def test_set_get
    UrlShortener.new
    @key = UrlShortener.set('http://www.google.com.my', 5)
    assert_not_equal nil, @key
    assert_equal 'http://www.google.com.my', UrlShortener.get(@key)
  end

end
