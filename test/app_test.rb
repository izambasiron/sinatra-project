require 'fakeredis'
require 'app'
require 'test/unit'
require 'rack/test'

class AppTest < Test::Unit::TestCase
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  def test_save_highscore
    post '/2048/highscores/ai?score=100&win=true'
    assert_equal 201, last_response.status
  end

  def test_get_scores
    get '/2048/highscores/ai'
    assert_equal 200, last_response.status
    assert_equal ({scores:[0]}.to_json), last_response.body
  end

  def test_get_win_count
    get '/2048/highscores/ai/win'
    assert_equal 200, last_response.status
    assert_equal ({win:0}.to_json), last_response.body
  end

  def test_get_loss_count
    get '/2048/highscores/ai/loss'
    assert_equal 200, last_response.status
    assert_equal ({loss:0}.to_json), last_response.body
  end

  def test_set_get
    post '/s?url=https://www.google.com.my'
    assert_equal 201, last_response.status
    assert_not_equal nil, last_response.body
    url = JSON.parse(last_response.body)['url']
    get url
    assert_equal 301, last_response.status
  end

end
